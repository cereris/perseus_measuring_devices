#ifndef ACTION_H
#define ACTION_H

#include "attributeListItem.h"
#include "functionListItem.h"

#ifdef __cplusplus
extern "C"
{
#endif

static const char REGISTRATION_REQUEST_TYPE_NAME[]= "RegistrationRequest";
static const char GET_REPLY_TYPE_NAME[]= "GetReply";
static const char CALL_REPLY_TYPE_NAME[]= "CallReply";

typedef struct {
    const char* source;
} RegistrationRequest;

typedef struct {
    const char* source;
    const char* target;
    AttributeListItem* attribute;
    enum Type type;
} GetReply;

typedef struct {
    const char* source;
    const char* target;
    FunctionListItem* function;
} CallReply;

#ifdef __cplusplus
}
#endif

#endif //ACTION_H
