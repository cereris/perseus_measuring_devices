#include "controlObject.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "action.h"
#include "jsonCodec.h"
#include "cJSON.h"
#include "attributeListItem.h"
#include "functionListItem.h"

void register_control_object(struct ControlObject* controlObject)
{
    RegistrationRequest request = {controlObject->name};
    char* message = registrationRequestToJSON(&request);
    controlObject->write(message);
    free(message);
}

void updateControlObject(struct ControlObject* controlObject)
{
    const char* receiveData = controlObject->read();
    if(strcmp(receiveData, HEART_BEAT_STRING) == 0)
    {
        if (controlObject->isRegister)
            controlObject->write(HEART_BEAT_STRING);
        else
            register_control_object(controlObject);

        return;
    }

    cJSON *jsonObject = cJSON_Parse(receiveData);
    if (jsonObject) {
        cJSON *moduleObject = cJSON_GetObjectItem(jsonObject, "__module__");
        cJSON *sourceObject = cJSON_GetObjectItem(jsonObject, "source");
        cJSON *classObject = cJSON_GetObjectItem(jsonObject, "__class__");

        if (moduleObject && sourceObject && classObject) {
            if (strcmp(moduleObject->valuestring, "perseus.control.action") == 0)
            {
                if(strcmp(classObject->valuestring, "RegistrationReply") == 0)
                {
                    cJSON *result = cJSON_GetObjectItem(jsonObject, "result");
                    if(result)
                        controlObject->isRegister = (bool)result->valueint;
                }
                else
                {
                    cJSON *targetObject = cJSON_GetObjectItem(jsonObject, "target");
                    if (targetObject)
                    {
                        if (strcmp(classObject->valuestring, "GetRequest") == 0)
                        {
                            cJSON *attributeObject = cJSON_GetObjectItem(jsonObject, "attribute_name");
                            struct AttributeListItem *attribute = findAttributeByName(controlObject,
                                                                                      attributeObject->valuestring);
                            if (attribute != NULL)
                            {
                                GetReply reply = {controlObject->name, sourceObject->valuestring,
                                                  attribute, attribute->type};
                                char *message = getReplyToJSON(&reply);
                                controlObject->write(message);
                                free(message);
                            }
                            else
                            {
                                char *message = getReplyWithExceptionToJSON(controlObject->name,
                                                                            sourceObject->valuestring,
                                                                            attributeObject->valuestring);
                                controlObject->write(message);
                                free(message);
                            }
                        }
                        else if (strcmp(classObject->valuestring, "CallRequest") == 0)
                        {
                            cJSON *methodObject = cJSON_GetObjectItem(jsonObject, "method_name");
                            FunctionListItem *function = findFunctionByName(controlObject, methodObject->valuestring);
                            if (function != NULL)
                            {
                                cJSON *argsObject = cJSON_GetObjectItem(jsonObject, "args");
                                if(argsObject !=NULL)
                                {
                                    if (argsObject->type == cJSON_Array)
                                    {
                                        if(cJSON_GetArraySize(argsObject) == 0) {

                                            CallReply reply = {controlObject->name, sourceObject->valuestring,
                                                               function};
                                            char *message = callReplyToJSON(&reply, 0);
                                            controlObject->write(message);
                                            free(message);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                char *message = callReplyWithExceptionToJSON(controlObject->name,
                                                                             sourceObject->valuestring,
                                                                             methodObject->valuestring);
                                controlObject->write(message);
                                free(message);
                            }
                        }
                    }
                }
            }
        }

        cJSON_Delete(jsonObject);
    }
}
