#include "controlObject.h"
#include "attributeListItem.h"
#include "functionListItem.h"
#include "Q2HX711.h"
#include "Ultrasonic.h"
#include "i2cmaster.h"


String buffer = "";
String temp = "";

Q2HX711 hx711(A1, A0);
Ultrasonic ultrasonic(12, 13);
double r0 = hx711.read();

static void write(const char* data){
  Serial.println(data);
}

static const char* read()
{
  while (Serial.available() > 0)
  {
    int charReading = Serial.read();
    buffer += (char)charReading;

    if (charReading == '\n') {
      temp = buffer;
      buffer = "";
      return temp.c_str();
    }
  }

  return "";
}

static ControlObject measuringDevicesObject;

double weight(void){
  double result = hx711.read();
  if(result < r0){
    double sum = 0;
    for(int i = 0; i < 5; i++){
      result = hx711.read();
      sum += (result - r0)/-23544.00;
    }
    return sum/5.0;
  }
  else{
    return 0;
  }
}

double growth(void){
  float dist_cm;
  double sum = 0;
  
  for(int i = 0; i < 5; i++){
      dist_cm = ultrasonic.Ranging(CM);
      sum += dist_cm;    
  }
    
   return (double)sum/5.0;
}

double temperature(void){
 
/*  int dev = 0x5A<<1;
  int data_low = 0;
  int data_high = 0;
  int pec = 0;
 
  Serial.println("a");
 
  i2c_start_wait(dev+I2C_WRITE);
  i2c_write(0x07);
  
  Serial.println("b");
    
  i2c_rep_start(dev+I2C_READ);
  data_low = i2c_readAck();
  data_high = i2c_readAck();
  pec = i2c_readNak();
  i2c_stop();
  
  Serial.println("c");
    
  double tempFactor = 0.02; 
  double tempData = 0x0000;
  int frac;
  
  Serial.println("d");
    
  tempData = (double)(((data_high & 0x007F) << 8) + data_low);
  tempData = (tempData * tempFactor)-0.01;
  double sum = 0, celcius;
    
  Serial.println("f");  
    
  for(int i = 0; i < 5; i++){
    celcius = tempData - 273.15;
    sum += celcius;
  }
  
  return sum/5.0;
  */
  
  double a = 36.6;
  
  return a;
  
}

void setup()
{
  Serial.begin(9600);
  
  i2c_init(); 
  PORTC = (1 << PORTC4) | (1 << PORTC5);
  
  measuringDevicesObject = {"MeasuringDevices", write, read, false};
  addFunction(&measuringDevicesObject, RETURN_DOUBLE_ARGUMENTS_VOID, "weight", &weight);
  addFunction(&measuringDevicesObject, RETURN_DOUBLE_ARGUMENTS_VOID, "growth", &growth);
  addFunction(&measuringDevicesObject, RETURN_DOUBLE_ARGUMENTS_VOID, "temperature", &temperature);
}

void loop()
{
  while(!measuringDevicesObject.isRegister)
  {
    register_control_object(&measuringDevicesObject);
    long int time = millis();
    while(millis()-time < 1000){
      updateControlObject(&measuringDevicesObject);
    }
  }

  while(true){
    updateControlObject(&measuringDevicesObject);
  }
}

