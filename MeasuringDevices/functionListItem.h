#ifndef FUNCTIONLIST_H
#define FUNCTIONLIST_H

#include <stdbool.h>

#include "type.h"

#ifdef __cplusplus
extern "C"
{
#endif

struct ControlObject;

struct FunctionListItem
{
    const char* functionName;
    double (*d)(void);
    const char* (*s)(void);
    struct FunctionListItem * next;
};

typedef struct FunctionListItem FunctionListItem;


void addFunction(struct ControlObject * controlObject, enum FunctionType type, const char* functionName, ...);
FunctionListItem * findFunctionByName(struct ControlObject * controlObject, const char* functionName);

#ifdef __cplusplus
}
#endif

#endif //FUNCTIONLIST_H
