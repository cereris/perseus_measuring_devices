#include "jsonCodec.h"

#include <stdlib.h>
#include <stdarg.h>

#include "cJSON.h"
#include "type.h"
#include "action.h"

char * registrationRequestToJSON(RegistrationRequest *request)
{
    cJSON* registrationRequest = cJSON_CreateObject();
    cJSON_AddItemToObject(registrationRequest, SOURCE_NAME, cJSON_CreateString(request->source));
    cJSON_AddItemToObject(registrationRequest, MODULE_NAME, cJSON_CreateString(ACTION_MODULE_NAME));
    cJSON_AddItemToObject(registrationRequest, CLASS_NAME, cJSON_CreateString(REGISTRATION_REQUEST_TYPE_NAME));
    char * resultString = cJSON_PrintUnformatted(registrationRequest);
    cJSON_Delete(registrationRequest);
    return resultString;
}

char* getReplyToJSON(GetReply* reply)
{
    cJSON* getReply = cJSON_CreateObject();
    cJSON_AddItemToObject(getReply, SOURCE_NAME, cJSON_CreateString(reply->source));
    cJSON_AddItemToObject(getReply, TARGET_NAME, cJSON_CreateString(reply->target));
    cJSON_AddItemToObject(getReply, ATTRIBUTE_NAME, cJSON_CreateString(reply->attribute->attributeName));
    cJSON_AddItemToObject(getReply, MODULE_NAME, cJSON_CreateString(ACTION_MODULE_NAME));
    cJSON_AddItemToObject(getReply, CLASS_NAME, cJSON_CreateString(GET_REPLY_TYPE_NAME));

    cJSON* result;

    switch(reply->type)
    {
        case DOUBLE:
            cJSON_AddItemToObject(getReply, RESULT_NAME, cJSON_CreateNumber(*(reply->attribute->d)));
            break;
    }
    char * resultString = cJSON_PrintUnformatted(getReply);
    cJSON_Delete(getReply);
    return resultString;
}

char* callReplyToJSON(CallReply *reply, int argsCount, ...)
{
    cJSON* callReply = cJSON_CreateObject();
    cJSON_AddItemToObject(callReply, SOURCE_NAME, cJSON_CreateString(reply->source));
    cJSON_AddItemToObject(callReply, TARGET_NAME, cJSON_CreateString(reply->target));
    cJSON_AddItemToObject(callReply, METHOD_NAME, cJSON_CreateString(reply->function->functionName));
    cJSON_AddItemToObject(callReply, MODULE_NAME, cJSON_CreateString(ACTION_MODULE_NAME));
    cJSON_AddItemToObject(callReply, CLASS_NAME, cJSON_CreateString(CALL_REPLY_TYPE_NAME));

    cJSON* result;

    if(!argsCount)
    {
        if(reply->function->d != NULL)
            cJSON_AddItemToObject(callReply, RESULT_NAME, cJSON_CreateNumber(reply->function->d()));
        else if(reply->function->s != NULL)
            cJSON_AddItemToObject(callReply, RESULT_NAME, cJSON_CreateString(reply->function->s()));
    }

    char * resultString = cJSON_PrintUnformatted(callReply);
    cJSON_Delete(callReply);
    return resultString;
}

char* getReplyWithExceptionToJSON(const char* source, const char* target, const char* attributeName)
{
    cJSON* getReply = cJSON_CreateObject();
    cJSON_AddItemToObject(getReply, SOURCE_NAME, cJSON_CreateString(source));
    cJSON_AddItemToObject(getReply, TARGET_NAME, cJSON_CreateString(target));
    cJSON_AddItemToObject(getReply, ATTRIBUTE_NAME, cJSON_CreateString(attributeName));
    cJSON_AddItemToObject(getReply, MODULE_NAME, cJSON_CreateString(ACTION_MODULE_NAME));
    cJSON_AddItemToObject(getReply, CLASS_NAME, cJSON_CreateString(GET_REPLY_TYPE_NAME));
    cJSON* result;
    cJSON_AddItemToObject(getReply, RESULT_NAME, result = cJSON_CreateObject());
    cJSON_AddItemToObject(result, MODULE_NAME, cJSON_CreateString(ACTION_MODULE_NAME));
    cJSON_AddItemToObject(result, CLASS_NAME, cJSON_CreateString(NO_RESPONSE_EXCEPTION_NAME));
    cJSON_AddItemToObject(result, "module_name", cJSON_CreateString(source));
    char * resultString = cJSON_PrintUnformatted(getReply);
    cJSON_Delete(getReply);
    return resultString;
}

char* callReplyWithExceptionToJSON(const char* source, const char* target, const char* methodName)
{
    cJSON* getReply = cJSON_CreateObject();
    cJSON_AddItemToObject(getReply, SOURCE_NAME, cJSON_CreateString(source));
    cJSON_AddItemToObject(getReply, TARGET_NAME, cJSON_CreateString(target));
    cJSON_AddItemToObject(getReply, METHOD_NAME, cJSON_CreateString(methodName));
    cJSON_AddItemToObject(getReply, MODULE_NAME, cJSON_CreateString(ACTION_MODULE_NAME));
    cJSON_AddItemToObject(getReply, CLASS_NAME, cJSON_CreateString(CALL_REPLY_TYPE_NAME));
    cJSON* result;
    cJSON_AddItemToObject(getReply, RESULT_NAME, result = cJSON_CreateObject());
    cJSON_AddItemToObject(result, MODULE_NAME, cJSON_CreateString(ACTION_MODULE_NAME));
    cJSON_AddItemToObject(result, CLASS_NAME, cJSON_CreateString(NO_RESPONSE_EXCEPTION_NAME));
    cJSON_AddItemToObject(result, "module_name", cJSON_CreateString(source));
    char * resultString = cJSON_PrintUnformatted(getReply);
    cJSON_Delete(getReply);
    return resultString;
}
