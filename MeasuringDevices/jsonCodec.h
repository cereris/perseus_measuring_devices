#ifndef JSONCODEC_H
#define JSONCODEC_H

#include "action.h"

#ifdef __cplusplus
extern "C"
{
#endif

static const char SOURCE_NAME[] = "source";
static const char TARGET_NAME[] = "target";
static const char MODULE_NAME[] = "__module__";
static const char ACTION_MODULE_NAME[] = "perseus.control.action";
static const char CLASS_NAME[] = "__class__";
static const char ATTRIBUTE_NAME[] = "attribute_name";
static const char METHOD_NAME[] = "method_name";
static const char RESULT_NAME[] = "result";
static const char NO_RESPONSE_EXCEPTION_NAME[] = "NoResponseException";


char* registrationRequestToJSON(RegistrationRequest *request);

char* getReplyToJSON(GetReply *reply);
char* getReplyWithExceptionToJSON(const char* source, const char* target, const char* attributeName);

char* callReplyToJSON(CallReply *reply, int argsCount, ...);
char* callReplyWithExceptionToJSON(const char* source, const char* target, const char* methodName);

#ifdef __cplusplus
}
#endif

#endif //JSONCODEC_H
