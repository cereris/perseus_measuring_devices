#ifndef CONTROLOBJECT_H
#define CONTROLOBJECT_H

#include <stdbool.h>
#include "functionListItem.h"

#ifdef __cplusplus
extern "C"
{
#endif

struct AttributeListItem;
struct FunctionListItem;

static const char HEART_BEAT_STRING[] = "#";

struct ControlObject
{
    const char* name;
    void (*write)(const char* data);
    const char* (*read)(void);
    bool isRegister;
    struct AttributeListItem * attributeList;
    struct FunctionListItem * functionList;
};

void register_control_object(struct ControlObject* controlObject);
void updateControlObject(struct ControlObject* controlObject);

#ifdef __cplusplus
}
#endif

#endif //CONTROLOBJECT_H
