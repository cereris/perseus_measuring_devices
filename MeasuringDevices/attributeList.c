#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "attributeListItem.h"
#include "controlObject.h"

void addAttribute(struct ControlObject * controlObject, enum Type type, const char* attributeName, ...)
{
    if(type == EXCEPTION)
        return;

    AttributeListItem * newHead = (AttributeListItem*)malloc(sizeof(AttributeListItem));
    newHead->attributeName = attributeName;
    newHead->next = NULL;
    newHead->d = NULL;
    newHead->type = type;

    va_list list;
    va_start(list, attributeName);

    switch(type)
    {
        case DOUBLE:
            newHead->d = va_arg(list, double*);
            break;
    }
    va_end(list);

    if(controlObject->attributeList == NULL)
        controlObject->attributeList = newHead;
    else
    {
        AttributeListItem * currentItem = controlObject->attributeList;
        while(currentItem->next != NULL)
            currentItem = currentItem->next;
        currentItem->next = newHead;
    }
}

AttributeListItem * findAttributeByName(struct ControlObject * controlObject, const char* attributeName)
{
    AttributeListItem * currentItem = controlObject->attributeList;
    while(currentItem != NULL)
    {
        if(strcmp(currentItem->attributeName, attributeName) == 0)
            return currentItem;

        currentItem = currentItem->next;
    }

    return NULL;
}
