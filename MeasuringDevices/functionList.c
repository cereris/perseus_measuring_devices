#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "functionListItem.h"
#include "controlObject.h"


void addFunction(struct ControlObject * controlObject, enum FunctionType type, const char* functionName, ...)
{
    FunctionListItem * item = findFunctionByName(controlObject, functionName);

    if(item == NULL)
    {
        item = (FunctionListItem *) malloc(sizeof(FunctionListItem));
        item->functionName = functionName;
        item->d = NULL;
        item->s = NULL;
        if (controlObject->functionList == NULL)
            controlObject->functionList = item;
        else
        {
            FunctionListItem *currentItem = controlObject->functionList;
            while (currentItem->next != NULL)
                currentItem = currentItem->next;
            currentItem->next = item;
        }
    }

    va_list list;
    va_start(list, functionName);
    switch(type)
    {
        case RETURN_DOUBLE_ARGUMENTS_VOID:
            item->d = va_arg(list, double(*)(void));
            break;
        case RETURN_STRING_ARGUMENTS_VOID:
            item->s = va_arg(list, const char*(*)(void));
            break;
    }
    va_end(list);
}

FunctionListItem * findFunctionByName(struct ControlObject * controlObject, const char* functionName)
{
    FunctionListItem * currentItem = controlObject->functionList;
    while(currentItem != NULL)
    {
        if(strcmp(currentItem->functionName, functionName) == 0)
            return currentItem;

        currentItem = currentItem->next;
    }

    return NULL;
}
