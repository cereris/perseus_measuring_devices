#ifndef ATTRIBUTELIST_H
#define ATTRIBUTELIST_H

#include <stdbool.h>

#include "type.h"

#ifdef __cplusplus
extern "C"
{
#endif

struct ControlObject;

struct AttributeListItem
{
    enum Type type;
    const char* attributeName;
    double * d;
    struct AttributeListItem * next;
};

typedef struct AttributeListItem AttributeListItem;

AttributeListItem * findAttributeByName(struct ControlObject * controlObject, const char* attributeName);
void addAttribute(struct ControlObject * controlObject, enum Type type, const char* attributeName, ...);



#ifdef __cplusplus
}
#endif

#endif //ATTRIBUTELIST_H
